#include <stdio.h>
#include <stdlib.h>
#include "priority.h"
#include "compare.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define DEBUG 0

/* A Fibonacci Node, containing:
 * - Its degree.
 * - Whether it's marked.
 * - Its key.
 * - Its value (void pointer; unknown type).
 * - Pointers to:
 *     - Its parent.
 *     - An arbitrary child.
 *     - Its left sibling.
 *     - Its right sibling.
 */
typedef struct FibNode {
	int marked;
	int degree;
	int key;
	void* value;
	struct FibNode* parent;
	struct FibNode* child;
	struct FibNode* left;
	struct FibNode* right;
}FibNode;

int fibNodeCount = 0;
FibNode* min = NULL;
FibNode* root = NULL;
FibNode** cons = NULL;
int order = 0;

/* Constructs a new Fibonacci node, given its `key` and `value`.
 */
struct FibNode* makeNode(int key, void* value) {
	FibNode* node = (FibNode*) malloc(sizeof(FibNode));
	node->marked = 0;
	node->degree = 0;
	node->parent = NULL;
	node->child = NULL;
	node->left = node;
	node->right = node;
	node->key = key;
	node->value = value;
	return node;
}

/* Prepares the heap for use, setting the required initial values.
 */
void makeHeap(){
	fibNodeCount = 0;
	min = NULL;
	root = NULL;
	cons = NULL;
	order = -1;
}

/* Deletes the heap, freeing its cons list and setting everything to NULL or 0.
 */
void deleteHeap(){
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (cons != NULL) {
		free(cons);
	}
	fibNodeCount = 0;
	FibNode* min = NULL;
	FibNode* root = NULL;
	FibNode** cons = NULL;
	int order = -1;
}

/* Returns a node with minimum value.
 * Returns NULL is there is no such node.
 */
void* findMin(){
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (min == NULL) {
		return NULL;
	} else {
		ret_t* node = (ret_t*) malloc(sizeof(ret_t));
		node->int_ptr = min;
		node->ext_ptr = min->value;
		node->weight = min->key;
		return node;
	}
}

/* Inserts `insertee` after `existing`, updating all pointers in O(1).
 * After means "to the right of" in this context.
 */
void insertAfter(FibNode* existing, FibNode* insertee) {
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (existing->right == existing) {
		existing->right = insertee;
		existing->left = insertee;
		insertee->right = existing;
		insertee->left = existing;
	} else {
		insertee->right = existing->right;
		existing->right->left = insertee;
		insertee->left = existing;
		existing->right = insertee;
	}
}

/* Inserts `insertee` before `existing`, updating all pointers in O(1).
 * Before means "after the left sibling of" in this context.
 */
void insertBefore(FibNode* existing, FibNode* insertee) {
	insertAfter(existing->left, insertee);
}

/* Inserts `node` into root list.
 */
void insertRootList(struct FibNode* node) {
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (root == NULL) {
		node->left = node;
		node->right = node;
		root = node;
	} else {
		insertAfter(root, node);
	}
}

/* Creates and inserts a new node, given its key and value.
 */
void* insert(int key, void* value){
	FibNode* _node = makeNode(key, value);
	insertRootList(_node);
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (min == NULL || min->key > _node->key) {
		min = _node;
	}
	fibNodeCount++;
	return _node;
}

/* Removes `node`, updating its pointers in O(1).
 * Returns the left sibling of node or NULL if no such sibling exists.
 * - If node has a parent, update its child pointer if needed.
 * - If node has siblings, update their sibling pointers as needed.
 */
struct FibNode* removeNode(struct FibNode* node) {
	struct FibNode* ret_node = NULL;
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (node->left == node) {
		ret_node = NULL;
	} else {
		ret_node = node->left;
	}

	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (node->parent != NULL && node->parent->child == node) {
		node->parent->child = ret_node;
	}

	node->right->left = node->left;
	node->left->right = node->right;

	node->parent = NULL;
	node->left = node;
	node->right = node;

	return ret_node;
}

/* Removes `node` from the root list.
 * Updates the pointers for root element as needed.
 */
void removeRootList(struct FibNode* node) {
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (node->left == node) {
		root = NULL;
	} else {
		root = removeNode(node);
	}
}

/* Links two heaps together, making `big` a child of `small`.
 */
void linkHeap(FibNode* big, FibNode* small) {
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (small->child == NULL) {
		small->child = big;
	} else {
		insertBefore(small->child, big);
	}

	big->parent = small;
	small->degree++;
	big->marked = 0;
}

/* Returns the next order of 2, needed by checkOrder.
 */
int nextPowerOf2(int size) {
	int order = 0;
    while((1<<order)<size) {
    	#ifdef COUNT_COMPARISON
			incComparisons();
		#endif
        order++;
    }
    return order;
}

/* Checks the order, ensuring the cons array is large enough to hold the needed
 * number of FibNodes.
 */
int checkOrder() {
	int old_order;
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (order == -1 || fibNodeCount > 1 << order) {
		old_order = order;
		order = nextPowerOf2(fibNodeCount + 1);
		#ifdef COUNT_COMPARISON
			incComparisons();
		#endif
		if (order < 10) {
			order = 10;
		}
		#ifdef COUNT_COMPARISON
			incComparisons();
		#endif
		if (order != old_order) {
			FibNode** tmp = (struct FibNode**)realloc(cons,
				sizeof(struct FibNode*)*(order + 1));
			cons = tmp;
			return 0;
		}
	}
}

/* This is the rather complex consolidation step. In high level terms, it
 * performs the following:
 * - Find two roots, `x` and `y` of the same degree and where
     `x->key` ≤ `y->key`.
 * - Link `x` and `y`, making `y` a child of `x`.
 * More detail is given in the comments.
 */
void consolidate() {
	// Initialize a lot of stuff.
	int i, d, D;
	struct FibNode* w = NULL;
	struct FibNode* x = NULL;
	struct FibNode* y = NULL;
	struct FibNode** array = NULL;

	// Ensure the cons array is large enough.
	checkOrder();

	// D is the maximum degree that can occur.
	D = order + 1;
	array = cons;

	// The array of root lists is initialized with NULL.
	for (i = 0; i < D; i++) {
		#ifdef COUNT_COMPARISON
			incComparisons();
		#endif
		array[i] = NULL;
	}

	// For all root lists:
	while ((w = root) != NULL) {
		#ifdef COUNT_COMPARISON
			incComparisons();
		#endif
		// Remove it from the root list.
		x = w;
		removeRootList(w);
		d = x->degree;
		// For all collisions of degree:
		while (array[d] != NULL) {
			#ifdef COUNT_COMPARISON
				incComparisons();
			#endif
			// Ensure `x->key` < `y->key`.
			y = array[d];
			#ifdef COUNT_COMPARISON
				incComparisons();
			#endif
			if (x->key > y->key) {
				struct FibNode* tmp = x;
				x = y;
				y = tmp;
			}
			// Link them.
			linkHeap(y, x);
			array[d] = NULL;
			d++;
		}
		array[d] = x;
	}

	// Insert all the lists into the root list.
	min = NULL;
	for (i = 0; i < D; i++) {
		#ifdef COUNT_COMPARISON
			incComparisons();
			incComparisons();
		#endif
		if (array[i] != NULL) {
			insertRootList(array[i]);
			// Update the pointer to `min` if necessary.
			#ifdef COUNT_COMPARISON
				incComparisons();
			#endif
			if (min == NULL || array[i]->key < min->key) {
				min = array[i];
			}
		}
	}
}

/* Deletes a node with minimum key value.
 * This operation is rather complex, as this is where the delayed consolidation
 * work must happen. The function performs the following tasks:
 * - Disowns all children of `min`, adding them to the root list.
 * - Removes `min` from the root list.
 * - Consolidates (which deserves a function in its own right).
 */
void deleteMin(){
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (min == NULL) {
		return;
	}
	struct FibNode* node = min;
	struct FibNode* first = NULL;
	struct FibNode* current = NULL;
	struct FibNode* next = NULL;
	for (current = node->child; current != first && current != NULL;) {
		#ifdef COUNT_COMPARISON
			incComparisons();
			incComparisons();
		#endif
		if (first == NULL) {
			first = current;
		}
		next = current->right;
		current->parent = NULL;
		insertRootList(current);
		current = next;
	}

	removeRootList(node);
	fibNodeCount--;
	
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (fibNodeCount == 0) {
		min = NULL;
	} else {
		min = node->right;
		consolidate();
	}
}

/* Cuts a node, disowning it and inserting it in the root list.
 */
void cut(FibNode* x, FibNode* y) {
	removeNode(x);
	y->degree--;
	insertRootList(x);
	x->parent = NULL;
	x->marked = 0;
}

/* Performs a cascading cut, cutting marked nodes, marking unmarked nodes.
 */
void cascadingCut(FibNode* node) {
	FibNode* p = node->parent;
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (p != NULL) {
		#ifdef COUNT_COMPARISON
			incComparisons();
		#endif
		if (node->marked == 0) {
			node->marked = 1;
		} else {
			cut(node, p);
			cascadingCut(p);
		}
	}
}

/* Decreases the key by a given amount. This operation incurs a cut.
 */
void decreaseKey(void* node, unsigned int w) {
	struct FibNode* _node = (FibNode*) node;
	_node->key = _node->key - w;

	FibNode* p = _node->parent;

	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (p != NULL && _node->key <= p->key) {
		cut(_node, p);
		cascadingCut(p);
	}

	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (_node->key < min->key) {
		min = _node;
	}
}

void printTitle(){
	printf("Fibonacci Heap");
}
