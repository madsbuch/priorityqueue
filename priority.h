typedef struct ret_t {
	void* int_ptr;//pointer to the internal element
	void* ext_ptr;//pointer to the external element
	int weight;   //weight of the node
} ret_t;


/**
 * initialise the heap, this function is called before any other
 * all data is just global, as it seams not a requirement, that we can create 
 * multiple heaps.
 */
void makeHeap();

/**
 * delete all structures. This should reset the state, so that the heap is ready
 * for another initialization
 * if nothing is intialised, it should just succeed
 */
void deleteHeap();

/**
 * return af pointer to the upper element of the heap.
 * this is just a pointer, it is up to he application programmer to figure out
 * what it points at
 * behaviour if empty: return nullpointer
 * so here a void* is actually a ret_t*, this type mismatch is to be fixed - eventually.
 */
void* findMin();

/**
 * insert an element a element with priority key
 * elements are represented by a pointer, what is points at, is up to the
 * application programmer
 * duplicate key should appear twice
 * what should happen on INT_MIN - something?
 */
void* insert(int key, void* node);

/**
 * deletes the one with the minimum key
 * if heap is empty, do nothing
 */
void deleteMin();

/**
 * decreses key k with the value of w.
 * the unsigned int effectily makes it impossible to try to increase k
 * undefined what element that is effected if k is duplicate
 */
void decreaseKey(void* element, unsigned int w);

/**
 * prints something to test headers
 */
void printTitle();
