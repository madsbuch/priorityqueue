/**
 * implementation of dijkstra's algorithm.
 *
 */
#include "graph.h"
#include "priority.h"
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

/********************* ACTUAL IMPLEMENTATION **********************************/

/**
 * creates a node on the heap, and returns its pointer
 */
graphNode* createNode(){
	graphNode* node = (graphNode*) malloc(sizeof(graphNode));
	
	//initialise the node
	node->maxEdges = 0;
	node->numEdges = 0;
	node->best = INT_MAX;
	node->heap_ptr = NULL;
	return node;
}

/**
 * obviousle the node should be heap allocated!
 */
/*void addNode(graphNode* node){
	//check if we have to allocate more space
	if(nodeCount >= maxNodes){
		//we just double
		if(maxNodes == 0)
			maxNodes = 50;
		maxNodes *= 2;
		nodes = (graphNode**) realloc(nodes, maxNodes*sizeof(graphNode*));
	}
	
	//prepare the node (let the datasetting be done in the connect function)
	node->maxEdges = 0;
	node->numEdges = 0;
	node->out = NULL;
	
	//do the actual add
	nodes[nodeCount] = node;
	nodeCount++;
}*/

/**
 * connects to nodes
 * in this implementation we use to way edges
 */
void connectNodes(graphNode* n1, graphNode* n2, int weight){
	
	//check for intial allocation
	if(n1->maxEdges == 0){
		n1->out = (graphEdge*) malloc(sizeof(graphEdge) * INITIAL_EDGE_COUNT);
		n1->maxEdges = INITIAL_EDGE_COUNT;
	}
	if(n2->maxEdges == 0){
		n2->out = (graphEdge*) malloc(sizeof(graphEdge) * INITIAL_EDGE_COUNT);
		n2->maxEdges = INITIAL_EDGE_COUNT;
	}
	
	//we reallocate here, and double the edgeCount
	if(n1->maxEdges <= n1->numEdges){
		n1->maxEdges *= 2;
		n1->out = (graphEdge*) realloc(n1->out, sizeof(graphEdge) * n1->maxEdges);
	}
	if(n2->maxEdges <= n2->numEdges){
		n2->maxEdges *= 2;
		n2->out = (graphEdge*) realloc(n2->out, sizeof(graphEdge) * n2->maxEdges);
	}
	
	//initialise edges
	n1->out[n1->numEdges].endpoint = n2;
	n1->out[n1->numEdges].len = weight;
	n1->numEdges++;
	
	n2->out[n2->numEdges].endpoint = n1;
	n2->out[n2->numEdges].len = weight;
	n2->numEdges++;
}

/**
 * Dijkstra algorithms
 * found here:
 * http://algorithms.soc.srcf.net/notes/dijkstra_with_heaps.pdf
 * (as this is only for performance, we don't need a from and to)
 */
void shortestPath(graphNode* allNodes[], int nodeCount){
	makeHeap();
	//insert all nodes into heap
	allNodes[0]->heap_ptr = insert(0, allNodes[0]);
	allNodes[0]->best = 0;
	for(int i=1;i<nodeCount ; i++){
		allNodes[i]->heap_ptr = insert(INT_MAX, allNodes[i]);
		allNodes[i]->best = INT_MAX;
	}
	
	/*
	FOR k from 1 to n:
		(i, d) := ExtractMin(Q)
		D[i] := d
		FOR all edges ij:
			IF d + w(i,j) < j’s key
				DecreaseKey(Q, node j, d + w(i,j))
	*/
	for(int i=0 ; i<nodeCount ; i++){
		//grap and pop the top element
		ret_t* ret = (ret_t*) findMin();
		deleteMin();
		int weight = ret->weight;
		graphNode* node = (graphNode*) ret->ext_ptr;
		//printf("node \"%s\" connections: %d best: %d\n", node->name, node->numEdges, node->best);
		for(int e=0 ; e < node->numEdges ; e++){
			graphEdge ge = node->out[e];
			//printf("if(%d < %d)\n", weight + ge.len , ge.endpoint->best);
			if(weight + ge.len < ge.endpoint->best){
				int decreaseWith = ge.endpoint->best - (weight + ge.len);
				//printf("decreasing \"%s\" with: %d (new: %d)\n",
				//	ge.endpoint->name,
				//	decreaseWith,
				//	ge.endpoint->best - decreaseWith);
				decreaseKey(ge.endpoint->heap_ptr, decreaseWith);
				ge.endpoint->best -= decreaseWith;
				//printf("new best %d\n", ge.endpoint->best);
			}
		}
	}

	
	deleteHeap();
}

/**
 * exporting graph for plotting, second priority
 */
char* exportGraph(){
	
}
