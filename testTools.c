#include <stdlib.h>
#include <time.h>

clock_t timer;

/**
 * not possible to get it more presice in POSIX?
 */
void startTimer(){
	timer = clock();
}
/**
 * returns the differense from startTime till now
 */
float getLapTime(){
	return (((float) (clock() -timer)) / CLOCKS_PER_SEC ); ;
}

/**
 * returns a pointer to n random ints (globally allocated)
 */
int* getRandomInts(int n){
	int* data = (int*) malloc(sizeof(int) * n);
	for(int i=0;i<n;i++){
		data[i] = rand();
	}
	return data;
}
