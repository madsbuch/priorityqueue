#include <stdio.h>
#include "priority.h"
#include "dijkstraTest.c"
#include <stdlib.h>
#include <limits.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"


//forward declerations of this files content
void integrityTest();
void performanceTest();
void dijkstraTest();

//integrity tests
void testInsertSorted();
void testInsertSortedReverseOrder();
void testMakeAndDeleteHeap();
void testDecreaseKey();
void testDeleteMin();
void testFindMinOnEmptyHeap();
void testDuplicateKeyAppearsTwice();
void testDeleteMinOnEmptyHeap();
void testInsertNullPointer();
void testDuplicateValue();
void testBorderValues();
void testDecreaseKeyNoOverflow();
void testNegativeKey();
void testRandomData();

//dijkstra
void testConnectNodes();
void testListGraph();
void testGridGraph();
void testGridPerformance(int n);
void testDensePerformance(int n);
void testSparseParformance(int n);

//performance
void performanceMakeHeap(int numTimes);
void performanceFindMin(int numobject);
void performanceInsert(int numobject);
void performanceDeleteMin(int numobject);
void performanceDecreaseKey(int numobject);

void testAll(){
	printf("Testing with implementation: ");
	printTitle();
	printf("\n");
	
	printf(ANSI_COLOR_RESET "Inegrity tests:\n" ANSI_COLOR_BLUE);
	integrityTest();
	printf("\n");
	
	printf("Testing Dijkstra:\n" ANSI_COLOR_BLUE);
	dijkstraTest();
	printf("\n");
	
	printf(ANSI_COLOR_RESET "Performance tests:\n" ANSI_COLOR_BLUE);
	performanceTest();
	printf("\n");
	
	printf(ANSI_COLOR_RESET);
}

/**
 * run all integrity tests
 */
void integrityTest(){
	testNegativeKey();
	testRandomData();
	testDeleteMinOnEmptyHeap();
	testInsertNullPointer();
	testDuplicateKeyAppearsTwice();
	testDuplicateValue();
	testFindMinOnEmptyHeap();
	testDeleteMin();
	testInsertSorted();
	testInsertSortedReverseOrder();
	testMakeAndDeleteHeap();
	testDecreaseKey();
	testBorderValues();
	testDecreaseKeyNoOverflow();
}

/**
 * run alle dijkstr tests
 */
void dijkstraTest(){
	testConnectNodes();
	testListGraph();
	testGridGraph();
	
	int itt = 10;
	
	//performance (malplaced)
	printf("Name , Size , Time\n");
	for(int i=0 ; i<itt ; i++)
		testGridPerformance(10*i);
	for(int i=0 ; i<itt ; i++)
		testDensePerformance(100*i);
	for(int i=0 ; i<itt ; i++)
		testSparseParformance(100*i);
}

void performanceTest(){
	int iterations = 10;
	for(int i=01 ; i<=iterations ; i++)
		performanceMakeHeap(100000*i);
	for(int i=01 ; i<=iterations ; i++)
		performanceFindMin(100000*i);
	for(int i=01 ; i<=iterations ; i++)
		performanceInsert(100000*i);
	for(int i=01 ; i<=iterations ; i++)
		performanceDeleteMin(100000*i);
	for(int i=01 ; i<=iterations ; i++)
		performanceDecreaseKey(100000*i);
}

/**
 * a very basic test, that tests if makeHeak and deleteHeap works
 */
void testMakeAndDeleteHeap(){
	int bogus1 = 42;
	int bogus2 = 84;
	
	makeHeap();
	insert(1, &bogus1);
	deleteHeap();//is supposed to remove all data
	
	makeHeap();
	insert(777, &bogus2);
	
	//priority of this element is higher, than from the heap from before
	//so it should be the new one that is returned
	ret_t* ret = (ret_t*) findMin();
	if(ret->ext_ptr == &bogus2)
		printf(ANSI_COLOR_GREEN "testMakeAndDeleteHeap succeeded\n" ANSI_COLOR_RESET);
	else
		printf(ANSI_COLOR_RED "testMakeAndDeleteHeap failed\n" ANSI_COLOR_RESET);
	
	deleteHeap();
}

/**
 * tests that a sorted input goes in and out correct
 */
void testInsertSorted(){
	makeHeap();
	//remember frame alocated array (automatic garbage collection!)
	int toInsert[] = {1, 2, 3, 4, 5, 6};
	int i = 0;
	//populating
	for(i=0 ; i<sizeof(toInsert) / sizeof(int) ; i++){
		int* ins = &toInsert[i];
		insert(toInsert[i], ins);
	}
	
	//testing
	for(i=0 ; i<sizeof(toInsert) / sizeof(int) ; i++){
		ret_t* t = (ret_t*) findMin();
		if(t->ext_ptr != &toInsert[i]){
			printf(ANSI_COLOR_RED "testInsertSorted failed\n" ANSI_COLOR_RESET);
			return;	
		}
		deleteMin();
	}
	
	printf(ANSI_COLOR_GREEN "testInsertSorted succeeded\n" ANSI_COLOR_RESET);
	
	deleteHeap();
}

void testInsertSortedReverseOrder(){

	makeHeap();
	//remember frame alocated array (automatic garbage collection!)
	int toInsert[] = {6, 5, 4, 3, 2, 1};
	int num = 6;
	int i = 0;
	//populating
	for(i=0 ; i<num ; i++){
		int* ins = &toInsert[i];
		insert(toInsert[i], ins);
	}
	
	//testing
	for(i=num-1 ; i>= 0 ; i--){
		ret_t* t = (ret_t*) findMin();
		if(t->ext_ptr != &toInsert[i]){
			printf(ANSI_COLOR_RED "testInsertSortedReverseOrder failed\n" ANSI_COLOR_RESET);
			return;	
		}
		deleteMin();
	}
	
	printf(ANSI_COLOR_GREEN "testInsertSortedReverseOrder succeeded\n" ANSI_COLOR_RESET);
	
	deleteHeap();
}

/**
 * tests if decreseKey works
 */
void testDecreaseKey(){
	int b1 = 42;
	int b2 = 140;
	
	makeHeap();
	
	void* n1 = insert(100, &b1);
	void* n2 = insert(50, &b2);
	//b2 should be on top of the heap now
	
	decreaseKey(n1, 70);//100 -> 30, be is decreased
	
	//b1 should be on top now
	ret_t* res = (ret_t*) findMin();
	if(res->ext_ptr == &b1)
		printf(ANSI_COLOR_GREEN "testDecreaseKey succeeded\n" ANSI_COLOR_RESET);
	else
		printf(ANSI_COLOR_RED "testDecreaseKey failed\n" ANSI_COLOR_RESET);
	
	deleteHeap();
}

void testDeleteMin(){
	makeHeap();
	//remember frame alocated array (automatic garbage collection!)
	int b1 = 42;
	int b2 = 8008;
	insert(1, &b1);
	insert(10, &b2);
	
	//before b1 was on top, after this action b2 should be on top
	deleteMin();
	
	ret_t* res = (ret_t*) findMin();
	if(res->ext_ptr == &b2)
		printf(ANSI_COLOR_GREEN "testDeleteMin succeeded\n" ANSI_COLOR_RESET);
	else
		printf(ANSI_COLOR_RED "testDeleteMin failed\n" ANSI_COLOR_RESET);
	deleteHeap();
}

void testFindMinOnEmptyHeap(){
	makeHeap();
	void* res = findMin();
	if(res == NULL)
		printf(ANSI_COLOR_GREEN "testFindMinOnEmptyHeap succeeded\n" ANSI_COLOR_RESET);
	else
		printf(ANSI_COLOR_RED "testFindMinOnEmptyHeap failed\n" ANSI_COLOR_RESET);
	deleteHeap();
	
}

void testDuplicateKeyAppearsTwice(){
	int bogus = 42;
	
	makeHeap();
	insert(10, &bogus);
	insert(10, &bogus);
	
	deleteMin();
	//we should still have 1 element in the queue
	ret_t* res = (ret_t*) findMin();
	if(res->ext_ptr == &bogus)
		printf(ANSI_COLOR_GREEN "testDuplicateKeyAppearsTwice succeeded\n" ANSI_COLOR_RESET);
	else
		printf(ANSI_COLOR_RED "testDuplicateKeyAppearsTwice failed\n" ANSI_COLOR_RESET);
}

void testDeleteMinOnEmptyHeap(){
	makeHeap();
	deleteMin();
	deleteMin();
	deleteMin();
	printf(ANSI_COLOR_GREEN "testDeleteMinOnEmptyHeap succeeded (checked implicitly)\n" ANSI_COLOR_RESET);
	deleteHeap();
}

/**
 * it should be possible to insert a null pointer?
 */
void testInsertNullPointer(){
	makeHeap();
	insert(42, NULL);
	ret_t* res = (ret_t*) findMin();
	if(res->ext_ptr == NULL)
		printf(ANSI_COLOR_GREEN "testInsertNullPointer succeeded\n" ANSI_COLOR_RESET);
	else
		printf(ANSI_COLOR_RED "testInsertNullPointer failed\n" ANSI_COLOR_RESET);

}

void testDuplicateValue(){
	makeHeap();
	insert(42, NULL);
	insert(44, NULL);
	ret_t* res = (ret_t*) findMin();
	if(res->ext_ptr == NULL)
		printf(ANSI_COLOR_GREEN "testDuplicateValue succeeded\n" ANSI_COLOR_RESET);
	else
		printf(ANSI_COLOR_RED "testDuplicateValue failed\n" ANSI_COLOR_RESET);
}

void testBorderValues(){
	makeHeap();
	int small = INT_MIN;
	int large = INT_MAX;

	insert(1, &small);
	insert(2, &large);

	ret_t* ret_s = (ret_t*) findMin();
	deleteMin();
	ret_t* ret_l = (ret_t*) findMin();

	if (ret_s->ext_ptr == &small && ret_l->ext_ptr == &large) {
		printf(ANSI_COLOR_GREEN "testBorderValues succeeded\n" ANSI_COLOR_RESET);
	} else {
		printf(ANSI_COLOR_RED "testBorderValues failed\n" ANSI_COLOR_RESET);
	}
}

void testDecreaseKeyNoOverflow(){
	makeHeap();
	int testMost = 34533;
	int testLeast = 47;
	
	insert(4, &testMost);
	void* theNode = insert(INT_MIN, &testLeast);
	decreaseKey(theNode, 80);
	
	//testLeast should be the element retrieved
	ret_t* val = (ret_t*) findMin();
	if(val->ext_ptr == &testLeast)
		printf(ANSI_COLOR_GREEN "testDecreaseKeyNoOverflow succeeded\n" ANSI_COLOR_RESET);
	else
		printf(ANSI_COLOR_RED "testDecreaseKeyNoOverflow failed\n" ANSI_COLOR_RESET);
	
	deleteHeap();
}

void testNegativeKey(){
	double testData = 354440.34;
	makeHeap();
	insert(-42, &testData);
	ret_t* res = (ret_t*) findMin();
	if(res->ext_ptr == &testData)
		printf(ANSI_COLOR_GREEN "testNegativeKey succeeded\n" ANSI_COLOR_RESET);
	else
		printf(ANSI_COLOR_RED "testNegativeKey failed\n" ANSI_COLOR_RESET);
	deleteHeap();
}

void testRandomData(){
	int num = 1000;
	int* data = getRandomInts(num);
	int last = INT_MIN;
	
	makeHeap();
	
	//inserting
	for(int i=0;i<num;i++)
		insert(data[i], &data[i]);
	for(int i=0;i<num;i++){
		ret_t* val = (ret_t*) findMin();
		if(!(val->weight >= last)){
			printf(ANSI_COLOR_RED "testRandomData failed\n" ANSI_COLOR_RESET);
			return;
		}
		last = val->weight;
		deleteMin();
	}
	printf(ANSI_COLOR_GREEN "testRandomData succeeded\n" ANSI_COLOR_RESET);
	
	free(data);
	
	deleteHeap();
}
