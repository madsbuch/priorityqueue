/**
 * This is a, hopefully, correct implementation based on the STL priority queue
 */
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <queue>
#include <stack>
#include "priority.h"

using namespace std;

typedef struct Node{
	void* element;
	int p;
}Node;

struct comp
{
    bool operator() (Node const &a, Node const &b) { return a.p > b.p; }
};

priority_queue<Node,std::vector<Node>, comp>* pq = NULL;

void makeHeap(){
	pq = new priority_queue<Node,std::vector<Node>, comp>();
}

void deleteHeap(){
	delete pq;
	pq = NULL;
}

void* findMin(){
	//check if empty
	if(pq->empty())
		return NULL;
	
	ret_t* ret = (ret_t*) malloc(sizeof(ret_t));
	ret->ext_ptr = pq->top().element;
	ret->int_ptr = pq->top().element;
	//not supported here
	ret->weight = 1;
	return ret;
}

void* insert(int index, void* node){
	Node n;
	n.element = node;
	n.p = index;
	pq->push(n);
	return node;
}

void deleteMin(){
	pq->pop();
}

/**
 * a wired workaround. pop all elements untill the element search for is on top
 * change key, pop it, and reinsert all other objects again and reinsert just changes
 * element (others is ordered, so we wna spare time)
 */
void decreaseKey(void* ele, unsigned int w){
	stack<Node> holder;
	Node theNode;
	while(true){
		//nothin happens
		if(pq->empty())
			return;
		
		//take the node out
		theNode = pq->top();
		pq->pop();
		
		//check if it is the one seearched for
		if(theNode.element == ele)
			break;
		//nah, we save it
		holder.push(theNode);
	}
	
	theNode.p = theNode.p - w;
	
	//add all saved nodes
	while(!holder.empty()){
		pq->push(holder.top());
		holder.pop();
	}
	
	//add the choosen!
	pq->push(theNode);
}

 void printTitle(){
        printf("Reference heap here!");
}
