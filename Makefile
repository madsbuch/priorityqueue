fibonacci:      main.c testTools.c fibonacciHeap.c test.c dijkstra.c performance.c
	g++ -std=c++0x -o main main.c testTools.c fibonacciHeap.c test.c dijkstra.c performance.c

fibonacciComparison:  compare.c fibonacciHeap.c main.c testTools.c test.c dijkstra.c performance.c
	g++ -D COUNT_COMPARISON -std=c++0x -o main compare.c fibonacciHeap.c main.c testTools.c test.c dijkstra.c performance.c

binary:         main.c testTools.c binaryHeap.c test.c dijkstra.c performance.c
	g++ -std=c++0x -o main main.c testTools.c binaryHeap.c test.c dijkstra.c performance.c

binaryComparison:     compare.c binaryHeap.c main.c testTools.c test.c dijkstra.c performance.c
	g++ -D COUNT_COMPARISON -std=c++0x -o main compare.c binaryHeap.c main.c testTools.c test.c dijkstra.c performance.c

ref: 			referenceHeap.cpp main.c testTools.c test.c dijkstra.c performance.c
	g++ -std=c++0x -o main referenceHeap.cpp main.c testTools.c test.c dijkstra.c performance.c

refComparisons: 		  referenceHeap.cpp main.c testTools.c test.c dijkstra.c performance.c
	g++ -std=c++0x -o main compare.c referenceHeap.cpp main.c testTools.c test.c dijkstra.c performance.c

clean:
	-@rm main 2>/dev/null || true
	
