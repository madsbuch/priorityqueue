#define INITIAL_EDGE_COUNT 100
//making it possible to be mutually dependent
typedef struct graphEdge graphEdge;
typedef struct graphNode graphNode;

struct graphNode {
	//This is built, so we don't have to reallocate space for the outedges
	//all the time
	graphEdge* out;
	int maxEdges;  // initialise this in the addNode function
	int numEdges;  // initialise this in the addNode function
	
	char name[10];//will probably easen up debuging
	
	//pointer to the best node, used in shortest path
	int best;
	
	//pointer to the heap
	void* heap_ptr;
};
struct graphEdge {
	//where do this not point to?
	graphNode* endpoint;
	//edgeWeight
	int len;
};

/**
 * some aux function
 */
//first node added is root node
graphNode* createNode();
void connectNodes(graphNode* n1, graphNode* n2, int weight);


/**
 * Dijkstra algorithms
 * dijstra is direveable from the best field, that should inductiely point from
 * the from node to the to node.
 */
void shortestPath(graphNode* allNodes[], int nodeCount);

/**
 * exporting graph for plotting, second priority
 */
//char* exportGraph();
