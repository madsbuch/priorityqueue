/**
 * dårlig stil at blande C og C++ i en og samme fil...
 * men pyt, vi skal være færdige
 */
#include <stdio.h>
#include <iostream>
#include "priority.h"
#include <stdlib.h>
#include <chrono>
#include "compare.h"

/* Timer granularity settings:
 * std::chrono::nanoseconds
 * std::chrono::microseconds
 * std::chrono::milliseconds
 * std::chrono::seconds
 * std::chrono::minutes
 * std::chrono::hours
 */

#define GRANULARITY std::chrono::microseconds
auto unit = "us";

//forward decleration, instead of making a .h 
void startTimer();
float getLapTime();
int* getRandomInts(int n);

/**
 * Performance testing makeHeap
 */
void performanceMakeHeap(int numTimes){
	auto start = std::chrono::system_clock::now();
	for(int i=0 ; i<numTimes ; i++){
		makeHeap();
		deleteHeap();
	}
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<GRANULARITY>(end - start);
	#ifdef COUNT_COMPARISON
	std::cout << "Comparisons for " << numTimes << " MakeHeap: " << getComparisons() << "\n";
	#else
	std::cout << "MakeHeap " << numTimes << " times in " << elapsed.count() << unit << "\n";
	#endif
}

/**
 * Performance testing findMin
 */
void performanceFindMin(int numobject){
	int data = 42;
	
	makeHeap();
	// allocate a lot of random elements, the nature of the elements is
	// irrelevant as we only work with pointer in the datastructures. 
	int* elements = getRandomInts(numobject);
	
	//insert
	for(int i=0 ; i<numobject ; i++){
		insert(elements[i], &data);
		//printf("inserted: %d\n", elements[i]);
	}
	
	//start timer
	auto start = std::chrono::system_clock::now();
	
	//remove elements
	//insert
	for(int i=0 ; i<numobject ; i++){
		findMin();
		//printf("inserted: %d\n", elements[i]);
	}
	
	//print laptime;
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<GRANULARITY>(end - start);
	#ifdef COUNT_COMPARISON
	std::cout << "Comparisons for " << numobject << " FindMin: " << getComparisons() << "\n";
	#else
	std::cout << "FindMin " << numobject << " times in " << elapsed.count() << unit << "\n";
	#endif
	
	free(elements);
	deleteHeap();
}

/**
 * Performancetesting inserts
 */
void performanceInsert(int numobject){
	int data = 42;
	
	makeHeap();
	//allocate a lot of random elements
	int* elements = getRandomInts(numobject);
	int* additions = getRandomInts(numobject);
	
	//start timer
	auto now = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<GRANULARITY>(now - now);

	//insert
	for(int i=0 ; i<numobject ; i++){
		insert(elements[i], &data);
		//printf("inserted: %d\n", elements[i]);
	}

	for (int i=0 ; i< numobject; i++) {
		auto start = std::chrono::system_clock::now();
		insert(additions[i], &data);
		auto end = std::chrono::system_clock::now();
		elapsed += std::chrono::duration_cast<GRANULARITY>(end - start);
		deleteMin();
	}

	
	//print laptime;
	#ifdef COUNT_COMPARISON
	std::cout << "Comparisons for " << numobject << " Insert: " << getComparisons() << "\n";
	#else
	std::cout << "Insert " << numobject << " times in " << elapsed.count() << unit << "\n";
	#endif

	free(elements);
	deleteHeap();
}

void performanceDeleteMin(int numobject){
	int data = 42;
	
	makeHeap();
	//allocate a lot of random elements
	int* elements = getRandomInts(numobject);
	int* additions = getRandomInts(numobject);
	
	//start timer
	auto now = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<GRANULARITY>(now - now);

	//insert
	for(int i=0 ; i<numobject ; i++){
		insert(elements[i], &data);
		//printf("inserted: %d\n", elements[i]);
	}

	for (int i=0 ; i< numobject; i++) {
		auto start = std::chrono::system_clock::now();
		deleteMin();
		auto end = std::chrono::system_clock::now();
		elapsed += std::chrono::duration_cast<GRANULARITY>(end - start);
		insert(additions[i], &data);
	}
	
	//print laptime;
	#ifdef COUNT_COMPARISON
	std::cout << "Comparisons for " << numobject << " DeleteMin: " << getComparisons() << "\n";
	#else
	std::cout << "DeleteMin " << numobject << " times in " << elapsed.count() << unit << "\n";
	#endif
	
	free(elements);
	deleteHeap();
}

/**
 * Performancetesting decreaseKey 
 */
void performanceDecreaseKey(int numobject){
	makeHeap();
	//allocate a lot of random elements
	int* elements = getRandomInts(numobject);
	void** pointers = (void**) malloc(sizeof(void*) * numobject);

	//insert
	for(int i=0 ; i<numobject ; i++){
		pointers[i] = insert(elements[i], elements+(i*sizeof(int)));
		//printf("inserted: %d\n", elements[i]);
	}

	//start timer
	auto start = std::chrono::system_clock::now();

	//remove elements
	//insert
	for(int i=0 ; i<numobject ; i++){
		decreaseKey(pointers[i], 1);
		//printf("inserted: %d\n", elements[i]);
	}

	//print laptime;
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<GRANULARITY>(end - start);
	#ifdef COUNT_COMPARISON
	std::cout << "Comparisons for " << numobject << " DecreaseKey: " << getComparisons() << "\n";
	#else
	std::cout << "DecreaseKey " << numobject << " times in " << elapsed.count() << unit << "\n";
	#endif

	free(elements);
	deleteHeap();
}
