/**
 * dårlig stil at blande C og C++ i en og samme fil...
 * men pyt, vi skal være færdige
 */
#include <stdio.h>
#include <chrono>
#include <iostream>
#include "graph.h"
#include <stdlib.h>


#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define GRANULARITY std::chrono::microseconds
auto unitDjk = "us";

//forward decleration, instead of making a .h 
void startTimer();
float getLapTime();
int* getRandomInts(int n);

void testConnectNodes(){
	graphNode* n1 = createNode();
	graphNode* n2 = createNode();
	connectNodes(n1, n2, 1);
	
	//printf("max: %d, num %d\n", n1->maxEdges, n1->numEdges);
	//printf("max: %d, num %d\n", n2->maxEdges, n2->numEdges);
	
	if(n1->out[0].endpoint == n2)
		printf(ANSI_COLOR_GREEN "testConnectNodes succeeded\n" ANSI_COLOR_RESET);
	else
		printf(ANSI_COLOR_RED "testConnectNodes failed\n" ANSI_COLOR_RESET);
}

void testListGraph(){
	int numNodes = 10;
	graphNode* gns[numNodes];
	
	//create 10 nodes
	for(int i=0 ; i<numNodes ; i++){
		gns[i] = createNode();
		sprintf(gns[i]->name, "node %d", i);
	}
	
	//connect them sequentally
	for(int i=0 ; i<numNodes-1 ; i++){
		//printf("%s and %s connected\n", gns[i]->name, gns[i+1]->name);
		connectNodes(gns[i], gns[i+1], 1);
	}
	
	//run dijkstra
	shortestPath(gns, numNodes);
	
	//test (best should be the next one)
	for(int i=0 ; i<numNodes-1 ; i++){
		//printf("best: %d\n", gns[i]->best);
		if(!(gns[i]->best == i)){
			printf(ANSI_COLOR_RED "testListGraph failed\n" ANSI_COLOR_RESET);
			return;
		}
	}
	
	//test that all nodes are in the shortest path
	printf(ANSI_COLOR_GREEN "testListGraph succeeded\n" ANSI_COLOR_RESET);
}

void testGridGraph(){
	int size = 10;
	graphNode* gns[size][size];
	graphNode* all[size*size];
	
	//create 10x10 nodes
	int i=0;
	for(int x=0 ; x < size ; x++){
		for(int y=0 ; y<size ; y++){
			gns[x][y] = createNode();
			sprintf(gns[x][y]->name, "node %d,%d", x, y);
			all[i] = gns[x][y];
			i++;
		}
	}
	
	//connect them in a grid
	for(int x=0 ; x < size-1 ; x++){
		for(int y=0 ; y < size-1 ; y++){
			//connect to rightmost and lower
			connectNodes(gns[x][y], gns[x+1][y], 1);
			connectNodes(gns[x][y], gns[x][y+1], 1);
		}
	}
	for(int i=0 ; i<size-1 ; i++){
		connectNodes(gns[size-1][i], gns[size-1][i+1], 1);
		connectNodes(gns[i][size-1], gns[i+1][size-1], 1);
	}
	
	//run dijkstra
	shortestPath(all, size*size);
	
	//the lower roght should have 2*size as best
	if(!(gns[size-1][size-1]->best == 2*(size-1))){
		printf(ANSI_COLOR_RED "testGridGraph failed\n" ANSI_COLOR_RESET);
		return;
	}
	
	//test that all nodes are in the shortest path
	printf(ANSI_COLOR_GREEN "testGridGraph succeeded\n" ANSI_COLOR_RESET);
}

/******************* WELL OK, PERFORMANCE SHOULD NOT BE HERE... ***************/

/**
 * be aware: the size is squared
 */
void testGridPerformance(int size){
	graphNode* gns[size][size];
	graphNode* all[size*size];
	
	//create 10x10 nodes
	int i=0;
	for(int x=0 ; x < size ; x++){
		for(int y=0 ; y<size ; y++){
			gns[x][y] = createNode();
			sprintf(gns[x][y]->name, "node %d,%d", x, y);
			all[i] = gns[x][y];
			i++;
		}
	}
	
	//connect them in a grid
	for(int x=0 ; x < size-1 ; x++){
		for(int y=0 ; y < size-1 ; y++){
			//connect to rightmost and lower
			connectNodes(gns[x][y], gns[x+1][y], 1);
			connectNodes(gns[x][y], gns[x][y+1], 1);
		}
	}
	for(int i=0 ; i<size-1 ; i++){
		connectNodes(gns[size-1][i], gns[size-1][i+1], 1);
		connectNodes(gns[i][size-1], gns[i+1][size-1], 1);
	}
	
	auto start = std::chrono::system_clock::now();
	
	//dijkstra
	shortestPath(all, size*size);
	
	//print laptime;
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<GRANULARITY>(end - start);
	std::cout << "testGridPerformance , " 
		<< size*size << " , " << elapsed.count() << unitDjk << "\n";
}
void testDensePerformance(int num){
	//ok with stack allocation, as thise frame is open while it's in use
	graphNode* all[num];
	//create initial node
	
	all[0] = createNode();
	for(int i=1 ; i<num ; i++){
		all[i] = createNode();
		//connect to all nodes
		for(int u=0 ; u<i ; u++)
			connectNodes(all[i], all[u], 1);
	}
	
	auto start = std::chrono::system_clock::now();
	
	//dijkstra
	shortestPath(all, num);
	
	//print laptime;
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<GRANULARITY>(end - start);
	std::cout << "testDensePerformance , " 
		<< num << " , " << elapsed.count() << unitDjk << "\n";
}
void testSparseParformance(int num){
	//ok with stack allocation, as thise frame is open while it's in use
	graphNode* all[num];
	//create initial node
	
	all[0] = createNode();
	for(int i=1 ; i<num ; i++){
		all[i] = createNode();
		//connect to a random existing node
		int r = rand() % i;
		connectNodes(all[i], all[r], 1);
	}
	
	auto start = std::chrono::system_clock::now();
	
	//dijkstra
	shortestPath(all, num);
	
	//print laptime;
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<GRANULARITY>(end - start);
	std::cout << "testSparseParformance , " 
		<< num << " , " << elapsed.count() << unitDjk << "\n";
}
