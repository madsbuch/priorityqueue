Priority Queue
==============

Description
-----------

[Her](http://www.cs.au.dk/~gerth/aa13/project1.html)

Make Targets
------------

Jeg anbefaler følgende linje til udvikling:

	make clean ; make {fibonacci, binary, ref} ; ./main

Clean er anbefalet, da programmet ikke kører hvis compilation failer

Testing fibonacci heap:

	make fibonacci

Testing binary heap:

	make binary

Testing with STL priority queue:

	make ref

Introduction
------------

Ændringer skal laves i 3 filer:

	test.c					(Mads tager denne)
	fibonacciHeap.c			(Emil tager denne)
	binaryHeap.c			(Jakob har fået denne)

Hver fil bliver tildelt en person, hvis der skal laves ændringer der involvere
flere end ens egen fil, skal vi lige snakke sammen om det, ellers risikere vi
at projektet falder fra hinanden.

interfaces til heaps er defineret i priority.h

