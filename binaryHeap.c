#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "priority.h"
#include <math.h>
#include "compare.h"

#define DEBUG 0

/*
* Binary heap node structure
* - key of the node
* - value pointer
* - index of the node in the tree array
* parent/left child/right child is calculated from the index.
*/
typedef struct binNode {
	int key;
	void* value;
	int index;
} binNode;

const int arrayInc = 2;

binNode* nodeArray;
int structSize = sizeof(binNode);
int insertedNodes;
int maxNodeArray;
intptr_t nodeArrayOffset;

int* tree;
int intPointerSize = sizeof(int*);
int node_count; // number of nodes in heap
int maxTree;

struct binNode* getTree(int i) {
	struct binNode* t = &nodeArray[tree[i]];
	return t;
}

void setTree(int i, int n) {
	tree[i] = n;
	getTree(i)->index = i;
}

/* Get the parent of the given node.
* Returns -1 if node is the root.
*/
int parent(int n) {
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (n == 0) { return -1; }
	return (int) floor((n - 1) / 2);
}

/* Get the left child of the given node.
* Returns -1 if no such exists.
*/
int left(int n) {
	int index = n * 2 + 1;
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (index >= node_count) { return -1; }
	return index;
}

/* Get the right child of the given node.
* Returns -1 if no such exists.
*/
int right(int n) {
	int index = n * 2 + 2;
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (index >= node_count) { return -1; }
	return index;
}

/* Get the child of the given node, that has the smallest key.
* If equal key, returns right child.
* Precondition: node has a left child.
*/
int minChild(int n) {
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (right(n) < 0) {
		return left(n);
	} else {
		return getTree(left(n))->key < getTree(right(n))->key ? left(n) : right(n);
	}
}

/* Swap the two given nodes.
*/
void swap(int p, int q) {
	// if the 'p == q' do nothing
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (p == q) { 
		return;
	}
	// swap positions in tree
	int tempIndex = tree[p];
	setTree(p, tree[q]);
	setTree(q, tempIndex);
}

void makeHeap() {
	node_count = insertedNodes = 0;
	maxTree = maxNodeArray = arrayInc;
	tree = (int*) malloc(arrayInc * intPointerSize);
	nodeArray = (binNode*) malloc(arrayInc * structSize);
	// treeOffset = (intptr_t) tree;
	nodeArrayOffset = (intptr_t) nodeArray;
}

void deleteHeap() {
	free(tree);
	free(nodeArray);
}

void decreaseKey(void* y, unsigned int k) {
	int x;
	intptr_t t = (intptr_t) y;
	x = (&nodeArray[t])->index;
	getTree(x)->key = getTree(x)->key - k;
	// while the node has a parent with a larger key, move the node one step up the tree.
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	while (parent(x) >= 0 && getTree(x)->key < getTree(parent(x))->key) {
		#ifdef COUNT_COMPARISON
			incComparisons();
			incComparisons();
		#endif
		swap(x, parent(x));
		x = parent(x);
	}
}

/* Increses key x with the value of k.
*/
void increaseKey(int x, unsigned int k) {
	getTree(x)->key = getTree(x)->key + k;
	int c;
	// while the node has a child with a smaller key, swap the node and the child
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	while (left(x) > 0) {
		#ifdef COUNT_COMPARISON
			incComparisons();
		#endif
		c = minChild(x);
		if (getTree(c)->key >= getTree(x)->key) { return; }
		swap(x, c);
		x = c;
	}
}

void* insert(int key, void* value) {
	// potentially allocate 
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (maxNodeArray <= insertedNodes) {
		maxNodeArray = maxNodeArray * arrayInc;
		nodeArray = (binNode*) realloc(nodeArray, maxNodeArray * structSize);
		nodeArrayOffset = (intptr_t) nodeArray;
	}
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (maxTree <= node_count) {
		maxTree = maxTree * arrayInc;
		tree = (int*) realloc(tree, maxTree * intPointerSize);
	}
	// insert node in tree
	// create node and insert
	struct binNode* node = &nodeArray[insertedNodes];
	node->key = key;
	node->value = value;
	node->index = node_count;
	
	setTree(node_count, insertedNodes);
	insertedNodes++;
	node_count++;
	
	struct binNode* diff = (struct binNode*) (node - nodeArray);
	decreaseKey(diff, 0);
	
	return diff;
}

void* findMin() {
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (node_count <= 0) {
		return NULL;
	} else {
		ret_t* node = (ret_t*) malloc(sizeof(ret_t));
		node->int_ptr = getTree(0);
		node->ext_ptr = getTree(0)->value;
		node->weight = getTree(0)->key;
		return node;
	}
}

void deleteMin() {
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (node_count <= 0) { return; }
	// swap the minimum element with the last (highest index) element
	swap(0, node_count - 1);
	// remove the last element by decrementing total number of elements
	node_count--;
	#ifdef COUNT_COMPARISON
		incComparisons();
	#endif
	if (node_count > 1) {
		// bobble the new root down the tree
		increaseKey(0, 0);
	}
}

void printTitle() {
	printf("Binary heap here!");
}